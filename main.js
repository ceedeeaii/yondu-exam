const axios = require('axios').default;
const fs = require('fs').promises;

const apiKey = 'a37f9e63-2fca-4cb6-b4f6-2a0d05bf5740';

const registerToExam = async () => {
  try {
    const requestEndpoint = 'https://goodmorning-axa-dev.azure-api.net/register';

    const requestBody = {
      Name: 'Carl Dennis M. Alingalan',
      Email: 'carlpoi08@gmail.com',
      Mobile: '09190950697',
      PositionApplied: 'NodeJS Developer',
      Source: 'Yondu',
    };

    const requestPayload = {
      url: requestEndpoint,
      method: 'post',
      data: JSON.stringify(requestBody),
      headers: { 'content-type': 'application/json' },
    };

    const responseResult = await axios(requestPayload);

    console.log(responseResult);
  } catch (error) {
    console.log(error);
  }
};

const uploadResume = async () => {
  try {
    const requestEndpoint = 'https://goodmorning-axa-dev.azure-api.net/upload';
    const resumeFileData = await fs.readFile('./resume.pdf', { encoding: 'base64' });

    const requestBody = { file: { mime: 'application/pdf', data: resumeFileData } };

    const requestPayload = {
      url: requestEndpoint,
      method: 'post',
      data: JSON.stringify(requestBody),
      headers: { 'content-type': 'application/json', 'x-axa-api-key': apiKey },
    };

    const responseResult = await axios(requestPayload);

    console.log(responseResult);
  } catch (error) {
    console.log(error);
  }
};

const bookTechnicalInterview = async () => {
  try {
    const requestEndpoint = 'https://goodmorning-axa-dev.azure-api.net/schedule';
    const requestBody = { ProposedDate: '2021-04-15', ProposedTime: '530PM', Online: true };

    const requestPayload = {
      url: requestEndpoint,
      method: 'post',
      data: JSON.stringify(requestBody),
      headers: { 'content-type': 'application/json', 'x-axa-api-key': apiKey },
    };

    const responseResult = await axios(requestPayload);

    console.log(responseResult);
  } catch (error) {
    console.log(error.response);
  }
};

(async () => {
  // await registerToExam;
  // await uploadResume();
  await bookTechnicalInterview();
})();
